#!/bin/bash

# Install KVM
# bridge-utils are not required but useful for debugging
apt-get install --no-install-recommends \
                qemu-system qemu-utils libvirt-clients libvirt-daemon-system ovmf bridge-utils
                
                
# Remove virbr0 NAT interface created during the kvm installation 
virsh net-destroy default
virsh net-undefine default


# Create network from exisiting host bridge
cat >/tmp/host-bridge.xml <<EOL
<network>
  <name>host-bridge</name>
  <forward mode="bridge"/>
  <bridge name="br0"/>
  <dns enable="no"/>
</network>
EOL
virsh net-define /tmp/host-bridge.xml
virsh net-autostart host-bridge
virsh net-start host-bridge
rm /tmp/host-bridge.xml


# Create isolated network (without DHCP)
cat >/tmp/isolated-bridge.xml <<EOL
<network>
  <name>isolated-bridge</name>
  <forward mode="bridge"/>
  <bridge name="br1"/>
   <dns enable="no"/>
</network>
EOL
virsh net-define /tmp/isolated-bridge.xml
virsh net-autostart isolated-bridge
virsh net-start isolated-bridge
rm /tmp/isolated-bridge.xml


# List networks
virsh net-list

# Allow admin to use libvirt
sudo usermod -a -G libvirt admin
