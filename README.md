# bootstrap
The script in this repo bootstraps Debian 12 „Bookworm“. The script may run in a grml live environment. Design decisions and usage instructions are outlined in the beginning of the script.


### Basic usage
```
git clone https://git.in-ulm.de/ulpeters/bootstrap.git
cd bootstrap
cp config.sh.template config.sh
vi config.sh
./bootstrap-bookworm.sh install
# reboot
# run as root
/installer/bootstrap-bookworm.sh postinstall
```

#### Run in grml live environment
1. Boot grml **amd64**
2. Press e to setup network if neccessary
3. Press ⏎ to run a shell
4. Run `Start ssh`, with a capital S to start the ssh server
5. Run `passwd` to change the root password
6. ./config-get-netconf-eth0.sh to copy running grml in `config.sh`
7. Login via ssh and proceed with *Basic usage*


#### Qemu disk as installation target
To prepare a qemu disk as installation target run following commands and set `disk=/dev/nbd0` in `config.sh`:
```
if ! lsmod | grep -q nbd; then sudo modprobe nbd max_part=8; fi # load nbd if missing
img="/var/lib/libvirt/images/ts-linux-testing.qcow2"
qemu-img create -f qcow2 -o size=20G $img  # create image
chown libvirt-qemu:libvirt-qemu $img       # make accessable for qemu
qemu-nbd -c /dev/nbd0 $img  # create device from image
qemu-nbd -d /dev/nbd0       # release device, after the installation is completed
```

#### Partition schema
- `partition=mbr-single`, recommended for small VMs, will prepare
  - a mbr partition table
  - a single partition formated as ext4 partition
  - install grub-pc
- `partition=efi-single`, recommended for small physical servers, will prepare
  - a gpt partion table
  - a 310 MB partition for efi
  - a 50 GB partition for the operation system
  - a partition with the remaining disk space for a luks containerapt install wireless-regdb

#### Missing Firmware
When the kernel reports missing
- firmware, run `apt install firmware-linux`
- regulatory.db, run `apt install wireless-regdb`
