#!/bin/bash -e

# Load configuration
[ -f /installer/config.sh ] && source /installer/config.sh
disk2=$disk"p3"


# 1. Open LUKS devices and sets up a mapping under /dev/mapper/
cryptsetup luksOpen $disk2 data
echo "Device mappings:" `find /dev/mapper/ -type l`

# 2. Mount LUKS volumes
mount --verbose /dev/mapper/data    /srv/data

# 3. Create a bind-mounts so we are not on the unecryped root volume
mount --verbose --bind /srv/data/guests/lib/docker   /var/lib/docker
mount --verbose --bind /srv/data/guests/docker       /opt/docker
mount --verbose --bind /srv/data/guests/lib/libvirt  /var/lib/libvirt
mount --verbose --bind /srv/data/guests/etc/libvirt  /etc/libvirt

# 4. Fix forward chain which was set to default deny by docker
# so our KVM containers on br0 have access to the network
iptables -v -A FORWARD -i br0 -o br0 -j ACCEPT

sleep 5 # wait a bit to make sure the data volume is ready

# 4. Start libvirt and docker 

# Those services should have been disabled
# systemctl disable docker libvirtd libvirt-guests
echo start libvirt
systemctl start libvirtd && sleep 2 && systemctl status --no-pager libvirtd

echo start libvirt-guests
systemctl start libvirt-guests && sleep 2 && systemctl status --no-pager libvirt-guests 

echo start docker
systemctl start docker && sleep 2 && systemctl status --no-pager docker


