# Migration Guide for NAS Server

- Base installation:
  - `bootstrap-bookworm.sh install`
  - Remove default network configuration
  - Copy /etc/systemd/network
  - Copy /home/admin
  - Adapt fstab: cat /etc/fstab | grep magnetic-backup >> /mnt/root/etc/fstab

- Initial boot in the new system
  - `bootstrap-bookworm.sh postinstall`
  - `apt-get install git man rsync` 
  - `prepare_luks.sh`
  - `install_kvm.sh`
  - `install_docker.sh`
  - Prepare for crypted data partition
    - `systemctl disable docker libvirtd libvirt-guests`, see startup.sh
    - Rename directories which will be bind-mounted from crypt data 
      ```
      mv /var/lib/docker /var/lib/docker.orig
      mkdir -p /opt/docker
      mv /var/lib/libvirt /var/lib/libvirt.orig
      mv /etc/libvirt /etc/libvirt.orig
      ```
    - Reboot
    - Check if services are disabled: `systemctl status docker libvirtd libvirt-guests`
    - Check if directories don't exist: `ls /var/lib/docker /var/lib/libvirt /etc/libvirt`
    - Prepare mount points: `mkdir -p /var/lib/docker /var/lib/libvirt /etc/libvirt`
    - Open luks and mount /srv/data, run manually step 1-2 in startup.sh
    - Create folder for bindmounts in /srv/data: `mkdir -p /srv/data/guests/lib/docker /srv/data/guests/docker /srv/data/guests/lib/libvirt /srv/data/guests/etc/libvirt`
    - Do bindmounts, run manually step 3 in startup.sh
    - rsync -a /var/lib/docker.orig/   /var/lib/docker
    - rsync -a /var/lib/libvirt.orig/  /var/lib/libvirt
    - rsync -a /etc/libvirt.orig/      /etc/libvirt
    - Start services, run manually step 4 in startup.sh
!!!!!!!!!
    - Test and delete .orig folders
    - /var/lib/docker auf crypt ziehen


- guest und magnetic einhängen, bind mounts
- pfsense und container testweise starten und prüfen
  - kvm und qemu stoppen
  - daten kopieren
  - virsh-define

- restart
- wurden die docker und kvm services wirklich nicht gestartet?
- startup.sh

- benchmark
- mailversand einrichten / prüfen: https://www.itix.fr/blog/send-mails-openwrt-msmtp-gmail/
- smartmon einrichten / testen
- power mgmt

- magnetic umziehen
  - docker stoppen
  - daten kopieren
  - docker container umbiegen - grep -r '/srv/magnetic' /opt/docker/
    - minio/restic
    - minio
    - sftp
    - samba
    - keepassxc

- backup prüfen
  - guests
cryptsetup luksOpen /dev/sdd guests
mount --verbose /dev/mapper/guests /mnt/old-guests/
rsync -a --info=progress2 /mnt/old-guests/docker/ /opt/docker
rsync -a --info=progress2 /mnt/old-guests/lib/libvirt/images/pfSense.qcow2 /mnt/old-guests/lib/libvirt/images/win10.qcow2 /var/lib/libvirt/images/
