#!/bin/bash

# Overwrite default variables from config file
[ -f /installer/config.sh ] && source /installer/config.sh


# Check if partition exists
disk2=$disk"p3"
if ! [ -b $disk2 ]
then
  echo $disk2 does not exist!
  exit
fi


mkdir -p /srv/data

cryptsetup luksFormat $disk2
cryptsetup luksOpen $disk2 data
mkfs.ext4 /dev/mapper/data
mount /dev/mapper/data /srv/data
