#!/bin/bash -e
## Install Docker
# https://docs.docker.com/engine/install/debian/

# Install dependencies
apt-get update && apt-get install --yes \
    ca-certificates \
    curl

# Add trust and docker apt repo
install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update

# Install docker
apt-get update && apt-get install --yes \
    docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# allow admin to use the docker command
usermod --append --groups docker admin 

#----------
# 1) Setup journald as log driver so we don't need to care separetly for log rotation
#
# 2) Change Docker Address pools
# Docker default address address pools:
# Vanilla
#    172.17.0.0/12 split in /16 (size) =>  16 networks each 65536 hosts
#   192.168.0.0/16 split in /24 (size) => 256 networks each   256 hosts
#   More details: # https://straz.to/2021-09-08-docker-address-pools/
# Reasons to change
#   - Short: The configuration is not suitable for our home server
#   - 192.168.0.0/16 is frequently used in the home environment 
#   - 172.17.0.0/12 size doesn't offer enough networks and we never have that many hosts
# Resulting configuraiton
#    172.16.0.0/12 split in /22 (size) => 1024 networks each 1024 hosts
#    nnnn nnnn . nnnn NNNN . NNNN NNhh . hhhh hhhh 
#                  12     2^10    22    2^10    32
# But, we start with 172.18.0.0 and
# (1)   leave 172.16.0.0/16 untouched
# (2.1) assign 172.17.0.0/22 to the default bridge
# (2.2) use the remaing networks for special purposes 172.17.4.0, 172.17.8.0, 172.17.12.0, ...
# => (1)+(2) takes each 64 /22-networks, resulting in 896 networks (1024-128)
#
# Tools: https://www.site24x7.com/tools/ipv4-subnetcalculator.html
# Reference: RFC 1918 name: 172.16.0.0 - 172.31.255.255
#
# Check configuration: docker info | grep Base
# Check configuration: docker network inspect bridge | grep Subnet
touch /etc/docker/daemon.json
cat >>/etc/docker/daemon.json <<EOL
{
  "log-driver": "journald",
  "bip": "172.17.0.1/22",
  "default-address-pools": [
    {
      "base": "172.18.0.0/12",
      "size": 20
    }
  ]
}
EOL
systemctl restart docker.service

# Create directory for docker
install --owner=root --group=docker --mode 775 --directory test /opt/docker

docker run hello-world
