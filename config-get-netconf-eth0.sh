#!/bin/bash -e

# Update config.sh with the running IPv4 grml network configuration for eth0

# Get predictiable alternative name as eth0 is not the default
echo netDev=\"`ip -fam inet addr show dev eth0 | grep -m1 -o 'altname .*' | cut -d" " -f2`\" >> config.sh
echo netAddress=\"`ip -fam inet addr show dev eth0 | grep -o 'inet .*' | cut -d" " -f2`\" >> config.sh
echo netGateway=\"`ip -fam inet route show dev eth0 | grep -o 'default .*' | cut -d" " -f3`\" >> config.sh
echo netBroadcast=\"`ip -fam inet addr show dev eth0 | grep -o 'inet .*' | cut -d" " -f2`\" >> config.sh
echo netDNS1=\"`cat /etc/resolv.conf | grep -m1 -o 'nameserver .*' | cut -d" " -f2`\" >> config.sh
echo netDNS2=\"`cat /etc/resolv.conf | grep -m2 -o 'nameserver .*' | tail -n1 | cut -d" " -f2`\" >> config.sh